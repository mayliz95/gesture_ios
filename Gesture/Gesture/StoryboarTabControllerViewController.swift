//
//  StoryboarTabControllerViewController.swift
//  Gesture
//
//  Created by Laboratorio FIS on 23/1/18.
//  Copyright © 2018 Laboratorio FIS. All rights reserved.
//

import UIKit

class StoryboarTabControllerViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let left = tabBar.items![0]
        let medio = tabBar.items![1]
        let rigth = tabBar.items![2]
        
        left.image = #imageLiteral(resourceName: "ic_motorcycle")
        medio.image = #imageLiteral(resourceName: "ic_receipt")
        rigth.image = #imageLiteral(resourceName: "ic_motorcycle")
        left.title = "Tap"
        medio.title = "Swipe"
        rigth.title = "Deber"
    }
}
