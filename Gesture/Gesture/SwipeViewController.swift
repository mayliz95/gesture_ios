//
//  SwipeViewController.swift
//  Gesture
//
//  Created by Laboratorio FIS on 17/1/18.
//  Copyright © 2018 Laboratorio FIS. All rights reserved.
//

import UIKit

class SwipeViewController: UIViewController {

    @IBOutlet weak var customeView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func SwipUPActionGesture(_ sender: UISwipeGestureRecognizer) {
        
        customeView.backgroundColor = .green
    }
    
    
    @IBAction func SwipDownActionGesture(_ sender: UISwipeGestureRecognizer) {
        customeView.backgroundColor = .red
    }
    
    @IBAction func SwipLeftActionGesture(_ sender: UISwipeGestureRecognizer) {
        customeView.backgroundColor = .black
    }
    
    @IBAction func SwipRigthActionGesture(_ sender: UISwipeGestureRecognizer) {
        
        customeView.backgroundColor = .yellow
    }
}
