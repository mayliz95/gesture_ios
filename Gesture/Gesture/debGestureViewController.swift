//
//  debGestureViewController.swift
//  Gesture
//
//  Created by Laboratorio FIS on 23/1/18.
//  Copyright © 2018 Laboratorio FIS. All rights reserved.
//

import UIKit

class debGestureViewController: UIViewController {

    
    @IBOutlet weak var customizeview: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func pinchGestureAction(_ sender: UIPinchGestureRecognizer) {
        customizeview.transform = customizeview.transform.scaledBy(x: sender.scale, y: sender.scale)
        sender.scale=1.0
        customizeview.backgroundColor = .yellow
    }
}
